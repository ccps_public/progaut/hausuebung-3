#ifndef PROGAUT_HAUSUEBUNG3_TESTHELPER_HPP
#define PROGAUT_HAUSUEBUNG3_TESTHELPER_HPP

#include <vector>

#include "Eigen/Dense"
#include "drvpath.hpp"

bool are_point_vectors_almost_equal(const std::vector<Point>& v1,
                                    const std::vector<Point>& v2,
                                    double tolerance);

std::vector<double> flat_matrix(const Eigen::MatrixXd& m);

std::vector<double> flat_vector(const Eigen::VectorXd& v);

bool are_double_vectors_almost_equal(const std::vector<double>& v1, const std::vector<double>& v2, double tolerance);

#endif  // PROGAUT_HAUSUEBUNG3_TESTHELPER_HPP
