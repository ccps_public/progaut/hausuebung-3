#include <iostream>

#include "drvpath.hpp"

int main() {
    std::vector<Point> lane{{2.0, -2.0},  {3.0, -2.0}, {4.5, -1.75},
                            {5.5, -1.5},  {6.5, -1.0}, {8, -0.25},
                            {10.0, 0.75}, {11.0, 1.5}};

    // std::vector<Point> lane{{0.5, -1.5}, {1.5, -1.5}, {2.5, -1.5}, {3.5,
    // -1.5},
    //                         {4.5, -1.5}, {5.5, -1.5}, {6.5, -1.5}, {7.5,
    //                         -1.5}};

    std::cout << "lane =";
    for (auto pt : lane) {
        std::cout << " (" << pt.x << ", " << pt.y << ")";
    }
    std::cout << "\n";

    // const auto path = get_car_path_points(lane, 1.5);

    // std::cout << "path =";
    // for (auto pt : path) {
    //     std::cout << " (" << pt.x << ", " << pt.y << ") ";
    // }
    // std::cout << "\n";

    // auto p = get_direct_path_length(path);
    // p = normalize_path_length(p, 2.0);

    // for (auto pl : p) {
    //     std::cout << pl << " ";
    // }
    // std::cout << "\n";

    // const auto x = get_coordinate(path, 0);
    // const auto theta = get_path_spline(p, x);

    // std::cout << "theta = ";
    // for (auto a : theta) {
    //     std::cout << a << " ";
    // }
    // std::cout << "\n";

    return 0;
}
