#include "drvpath.hpp"

#include <cmath>

#include "Eigen/Dense"

std::vector<Point> get_car_path_points(const std::vector<Point>& lane,
                                       double distance) {
    return {};
}

std::vector<double> get_direct_path_length(const std::vector<Point>& path) {
    return {};
}

std::vector<double> normalize_path_length(std::vector<double> path_length,
                                          double final_value){
    return {};
}

std::vector<double> get_coordinate(const std::vector<Point>& path,
                                   size_t index) {
    return {};
}

std::pair<Eigen::MatrixXd, Eigen::VectorXd> get_objective_Psi_eta(
    const std::vector<double>& p, const std::vector<double>& z) {
    return {};
}

std::pair<Eigen::MatrixXd, Eigen::VectorXd> get_constraints_A_b() {

    return {};
}

std::pair<Eigen::MatrixXd, Eigen::VectorXd> get_quadprog_L_r(
    const Eigen::MatrixXd& Psi, const Eigen::VectorXd& eta,
    const Eigen::MatrixXd& A, const Eigen::VectorXd& b) {
    return {};
}

std::vector<double> get_path_spline(const std::vector<double>& p,
                                    const std::vector<double>& z) {
    return {};
}
