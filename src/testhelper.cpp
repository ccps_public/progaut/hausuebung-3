#include "testhelper.hpp"

bool are_point_vectors_almost_equal(const std::vector<Point>& v1,
                                    const std::vector<Point>& v2,
                                    double tolerance) {
    if (v1.size() != v2.size()) {
        return false;
    }

    for (size_t i = 0; i < v1.size(); ++i) {
        const double dx = v1[i].x - v2[i].x;
        const double dy = v1[i].y - v2[i].y;

        if ((std::abs(dx) > tolerance) || (std::abs(dy) > tolerance)) {
            return false;
        }
    }

    return true;
}

std::vector<double> flat_matrix(const Eigen::MatrixXd& m) {
    const int rows = m.rows();
    const int cols = m.cols();

    std::vector<double> res{};
    res.reserve(rows * cols);

    for (int r = 0; r < rows; ++r) {
        for (int c = 0; c < cols; ++c) {
            res.push_back(m(r, c));
        }
    }

    return res;
}

std::vector<double> flat_vector(const Eigen::VectorXd& v) {
    return {v.begin(), v.end()};
}
bool are_double_vectors_almost_equal(const std::vector<double>& v1, const std::vector<double>& v2, double tolerance) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (std::abs(v1[i] - v2[i]) > tolerance) {
            return false;
        }
    }
    return true;
}